import 'dart:math';
import 'package:flutter/material.dart';

class RandomSizeModeWidget extends StatefulWidget {
  const RandomSizeModeWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<RandomSizeModeWidget> createState() => _RandomSizeModeWidget();
}

class _RandomSizeModeWidget extends State<RandomSizeModeWidget> {
  Color color = Colors.deepOrange;
  double width = 100;
  double height = 100;

  static const int _colorRGB = 256;
  static const int _minSize = 50;
  static const int _maxSize = 400;

  void colorPicker() {
    setState(() {
      color = Color.fromRGBO(
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        1,
      );
      width = Random().nextInt(_maxSize) + _minSize.toDouble();
      height = Random().nextInt(_maxSize) + _minSize.toDouble();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: colorPicker,
        child: Center(
          child: AnimatedContainer(
            color: color,
            height: height,
            width: width,
            duration: const Duration(milliseconds: 500),
            child: const Center(
              child: Text(
                'HEY THERE!',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                  fontWeight: FontWeight.w800,
                  fontFamily: 'BebasNeue',
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
