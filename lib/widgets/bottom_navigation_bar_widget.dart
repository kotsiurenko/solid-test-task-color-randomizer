import 'package:flutter/material.dart';

class BottomNavigationBarWidget extends StatefulWidget {
  final PageController pageController;

  const BottomNavigationBarWidget({Key? key, required this.pageController})
      : super(key: key);

  @override
  State<BottomNavigationBarWidget> createState() =>
      _BottomNavigationBarWidget();
}

class _BottomNavigationBarWidget extends State<BottomNavigationBarWidget> {
  _PageEnum selectIndex = _PageEnum.fullscreenMode;
  static const double _bottomBarSize = 70;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: _bottomBarSize,
      padding: const EdgeInsets.all(12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _AppBarInfo(
            title: "FullScreen Mode",
            icon: Icons.fit_screen,
            isSelect: selectIndex == _PageEnum.fullscreenMode,
            onTap: () => onTap(_PageEnum.fullscreenMode),
          ),
          _AppBarInfo(
            title: 'Random Size Mode',
            icon: Icons.photo_size_select_small,
            isSelect: selectIndex == _PageEnum.randomSizeMode,
            onTap: () => onTap(_PageEnum.randomSizeMode),
          ),
          _AppBarInfo(
            title: 'Double Color Mode',
            icon: Icons.color_lens,
            isSelect: selectIndex == _PageEnum.doubleColorMode,
            onTap: () => onTap(_PageEnum.doubleColorMode),
          ),
        ],
      ),
    );
  }

  void onTap(_PageEnum index) {
    setState(() {
      selectIndex = index;
      widget.pageController.animateToPage(
        selectIndex.index,
        duration: const Duration(milliseconds: 500),
        curve: Curves.linear,
      );
    });
  }
}

class _AppBarInfo extends StatelessWidget {
  final String title;
  final IconData icon;
  final bool isSelect;
  final void Function() onTap;
  static const double _iconSize = 30;

  const _AppBarInfo({
    Key? key,
    required this.title,
    required this.icon,
    required this.onTap,
    this.isSelect = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(
            icon,
            color: isSelect ? Colors.blueAccent : Colors.grey,
            size: _iconSize,
          ),
          Text(
            title,
            style: const TextStyle(
              color: Colors.grey,
              fontSize: 8,
              fontWeight: FontWeight.w500,
            ),
          )
        ],
      ),
    );
  }
}

enum _PageEnum {
  fullscreenMode,
  randomSizeMode,
  doubleColorMode,
}
