import 'dart:math';
import 'package:flutter/material.dart';

class FullscreenModeWidget extends StatefulWidget {
  const FullscreenModeWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<FullscreenModeWidget> createState() => _FullscreenModeWidget();
}

class _FullscreenModeWidget extends State<FullscreenModeWidget> {
  Color color = Colors.yellow;
  Color textColor = Colors.blueAccent;
  static const int _colorRGB = 256;
  static double _fontSize = 30;
  static const int _fontMinSize = 15;
  static const int _fontMaxSize = 50;

  void colorPicker() {
    setState(() {
      color = Color.fromRGBO(
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        1,
      );
      textColor = Color.fromRGBO(
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        1,
      );
      _fontSize = Random().nextInt(_fontMaxSize) + _fontMinSize.toDouble();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: colorPicker,
        child: AnimatedContainer(
          color: color,
          duration: const Duration(milliseconds: 500),
          child: Center(
            child: Text(
              'HEY THERE!',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: _fontSize,
                color: textColor,
                fontWeight: FontWeight.w800,
                fontFamily: 'BebasNeue',
              ),
            ),
          ),
        ),
      ),
    );
  }
}
