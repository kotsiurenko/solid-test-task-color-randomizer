import 'dart:math';
import 'package:flutter/material.dart';

class DoubleColorModeWidget extends StatefulWidget {
  const DoubleColorModeWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<DoubleColorModeWidget> createState() => _DoubleColorModeWidget();
}

class _DoubleColorModeWidget extends State<DoubleColorModeWidget> {
  Color color = Colors.yellow;
  Color bgColor = Colors.blueAccent;
  Color circleColor = Colors.blueAccent;
  double width = 200;
  double height = 200;

  static const int _colorRGB = 256;
  static const int _minSize = 150;
  static const int _maxSize = 350;
  static const double _blurRadius = 5.0;
  static const double _spreadRadius = 5.0;

  void colorPicker() {
    setState(() {
      width = Random().nextInt(_maxSize) + _minSize.toDouble();
      height = Random().nextInt(_maxSize) + _minSize.toDouble();
      color = Color.fromRGBO(
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        1,
      );
      bgColor = Color.fromRGBO(
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        1,
      );
      circleColor = Color.fromRGBO(
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        Random().nextInt(_colorRGB),
        1,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: InkWell(
          onTap: colorPicker,
          child: AnimatedContainer(
            duration: const Duration(seconds: 1),
            color: bgColor,
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: AnimatedContainer(
                duration: const Duration(seconds: 1),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: circleColor,
                      blurRadius: _blurRadius,
                      spreadRadius: _spreadRadius,
                    )
                  ],
                  color: color,
                ),
                width: width,
                height: height,
                child: const Center(
                  child: Text(
                    'HEY THERE!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.white,
                      fontWeight: FontWeight.w800,
                      fontFamily: 'BebasNeue',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
