import 'package:color_randomizer/widgets/bottom_navigation_bar_widget.dart';
import 'package:color_randomizer/widgets/double_color_mode_widget.dart';
import 'package:color_randomizer/widgets/fullscreen_mode_widget.dart';
import 'package:color_randomizer/widgets/random_size_mode_widget.dart';
import 'package:flutter/material.dart';

class MaterialAppWidget extends StatelessWidget {
  final PageController _pageController = PageController();

  MaterialAppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: PageView(
          controller: _pageController,
          physics: const NeverScrollableScrollPhysics(),
          children: const [
            FullscreenModeWidget(),
            RandomSizeModeWidget(),
            DoubleColorModeWidget(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBarWidget(
          pageController: _pageController,
        ),
      ),
    );
  }
}
