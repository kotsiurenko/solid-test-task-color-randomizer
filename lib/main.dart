import 'package:color_randomizer/widgets/material_app_widget.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialAppWidget());
